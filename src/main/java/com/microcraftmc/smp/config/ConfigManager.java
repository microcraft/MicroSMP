package com.microcraftmc.smp.config;

import com.microcraftmc.smp.MicroSMP;
import com.microcraftmc.smp.utils.files.FileUtils;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.logging.Level;

public class ConfigManager {

    private static ConfigManager instance;

    public static ConfigManager getInstance() {
        if (instance == null) {
            instance = new ConfigManager();
        }
        return instance;
    }

    //config
    private static FileConfiguration config;
    private File configFile;

    //messages
    private static FileConfiguration messages;
    private File messageFile;

    //discord config
    private static FileConfiguration discord;
    private File discordFile;

    public void setup() {

        //copy default config file
        MicroSMP.get().getLogger().info("Loading configs");

        try {

            if (!MicroSMP.get().getDataFolder().exists()) {
                MicroSMP.get().getDataFolder().mkdir();
            }

            configFile = new File(MicroSMP.get().getDataFolder(), "config.yml");
            if (!configFile.exists()) {
                //copy default config file and defaults
                FileUtils.loadFile("config.yml");
            }

            messageFile = new File(MicroSMP.get().getDataFolder(), "messages.yml");
            if (!messageFile.exists()) {
                //save default kits file
                FileUtils.loadFile("messages.yml");
            }

            discordFile = new File(MicroSMP.get().getDataFolder(), "discord.yml");
            if (!discordFile.exists()) {
                //create a new empty file
                discordFile.createNewFile();
                MicroSMP.get().getLogger().log(Level.INFO, "Created new config: " + discordFile.getName());
            }

            reloadConfig();
            reloadMessages();
            reloadDiscord();

        } catch (Exception ex) {
            ex.printStackTrace();
            MicroSMP.get().getLogger().severe("Could not load configs");
        }

    }

    public static FileConfiguration getConfig() {
        return config;
    }

    public static FileConfiguration getMessages() {
        return messages;
    }

    public static FileConfiguration getDiscord() {
        return discord;
    }

    public void reloadConfig() {
        try {

            config = new YamlConfiguration();
            config.load(configFile);

        } catch (Exception ex) {
            MicroSMP.get().getLogger().severe("Could not reload config: " + configFile.getName());
        }
    }

    public void reloadMessages() {
        try {

            messages = new YamlConfiguration();
            messages.load(messageFile);

        } catch (Exception ex) {
            MicroSMP.get().getLogger().severe("Could not reload config: " + messageFile.getName());
        }
    }

    public void reloadDiscord() {
        try {

            discord = new YamlConfiguration();
            discord.load(discordFile);

        } catch (Exception ex) {
            MicroSMP.get().getLogger().severe("Could not reload config: " + messageFile.getName());
        }
    }

    public void saveConfig() {
        try {
            config.save(configFile);
        } catch (Exception ex) {
            MicroSMP.get().getLogger().severe("Could not save config: " + configFile.getName());
        }
    }

    public void saveMessages() {
        try {
            messages.save(messageFile);
        } catch (Exception ex) {
            MicroSMP.get().getLogger().severe("Could not save config: " + messageFile.getName());
        }
    }

    public void saveDiscord() {
        try {
            discord.save(discordFile);
        } catch (Exception ex) {
            MicroSMP.get().getLogger().severe("Could not save config: " + messageFile.getName());
        }
    }

}
