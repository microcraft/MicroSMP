package com.microcraftmc.smp.tasks;

import com.microcraftmc.smp.bossbar.BarManager;
import com.microcraftmc.smp.discord.DiscordBot;
import com.microcraftmc.smp.discord.utils.ChannelLogger;
import com.microcraftmc.smp.utils.dates.DateUtils;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.scheduler.BukkitRunnable;

public class UpdateCountdownTask extends BukkitRunnable {

    private static UpdateCountdownTask instance;
    private static int countdown = 1 * 60 + 1;

    public static UpdateCountdownTask getInstance() {
        if (instance == null) {
            instance = new UpdateCountdownTask();
        }
        return instance;
    }

    private UpdateCountdownTask() {
        countdown = 1 * 60 + 1;
    }

    @Override
    public void run() {
        countdown = countdown - 1;
        BarManager.getAnnounceBar().setVisible(true);

        if (countdown <= 10) {
            if (DiscordBot.getInstance().isEnabled()) {
                ChannelLogger.logStatusMessage("Shutting down for updates in " + DateUtils.secondsToString(countdown));
            }
        }

        if (countdown <= 0){
            this.cancel();
            Bukkit.getServer().shutdown();
        }

        BarManager.getAnnounceBar().setTitle(ChatColor.DARK_RED + "Shutting down for updates in " + ChatColor.GRAY + DateUtils.secondsToString(countdown));
        BarManager.getAnnounceBar().setProgress((float)countdown/(float)(60 * 1));
        BarManager.getAnnounceBar().setColor(BarColor.BLUE);

    }


}
