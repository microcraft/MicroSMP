package com.microcraftmc.smp.user;

import com.microcraftmc.smp.MicroSMP;
import com.microcraftmc.smp.bossbar.BarManager;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class UserManager {

    private static UserManager instance;

    private ConcurrentMap<UUID, User> userMap = new ConcurrentHashMap<>();

    public static UserManager getInstance() {
        if (instance == null) {
            instance = new UserManager();
        }
        return instance;
    }

    public ConcurrentMap<UUID, User> getUserMap() {
        return userMap;
    }

    public Collection<User> getUsers(){
        return userMap.values();
    }

    public User getUser(Player player) {
        return  player == null ? null : userMap.get(player.getUniqueId());
    }

    public User procesJoin(Player p){
        User user = getUserMap().get(p.getUniqueId());

        if(user == null){
            user = new User(p.getUniqueId());
            getUserMap().put(p.getUniqueId(), user);
        }

        user.setPlayer(p);
        user.setPermissionUser(user.getPermissionUser());
        BarManager.getAnnounceBar().addPlayer(p);

        return user;
    }

    public void processLeave(Player p) {

        //remove the user from the UserMap and the AnnounceBar
        getUserMap().remove(p.getUniqueId());
        BarManager.getAnnounceBar().removePlayer(p);

    }

}
