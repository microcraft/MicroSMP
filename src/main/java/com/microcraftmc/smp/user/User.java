package com.microcraftmc.smp.user;

import com.comphenix.packetwrapper.WrapperPlayServerPlayerListHeaderFooter;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.microcraftmc.smp.MicroSMP;
import com.microcraftmc.smp.config.ConfigManager;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.io.File;
import java.util.*;
import java.util.logging.Level;

public class User {

    private final UUID uuid;
    private String name = null;
    private Player player = null;
    private PermissionUser permissionUser = null;
    private BossBar bossBar = Bukkit.createBossBar("", BarColor.PURPLE, BarStyle.SOLID);
    private BukkitRunnable bossBarCallBack = null;

    private FileConfiguration config;
    private File userFile;

    private boolean creating = false;

    //mic stuff
    private Map<String, Object> homes;

    public User(UUID uuid) {
        this.uuid = uuid;

        //check if user file exists
        userFile = new File(MicroSMP.get().getUserFolder(), uuid + ".yml");
        if (!userFile.exists()) {
            try {
                userFile.createNewFile();
                creating = true;
            } catch (Exception ex) {
                MicroSMP.get().getLogger().severe("Could not create data file: " + ex.getMessage());
            }
        }

        //load users config
        try {
            config = new YamlConfiguration();
            config.load(userFile);
        } catch (Exception ex) {
            MicroSMP.get().getLogger().severe("Could not load user data file: " + ex.getMessage());
        }

        //do we setup the user?
        if (creating) {
            player = Bukkit.getPlayer(uuid);

            if (player != null) {
                config.set("username", player.getName());
                config.set("uuid", player.getUniqueId().toString());
                config.set("ip", player.getAddress().getAddress().getHostAddress());
            }

            config.set("firstjoined", new Date().getTime());
            config.set("lastlogin", new Date().getTime());
            config.set("lastlogout", -1l);

            config.set("muted.status", false);
            config.set("muted.reason", "NOT_MUTED");
            config.set("muted.time", -1);

            saveConfig();
        }

        bossBar.setVisible(false);

    }

    public String getMessagePrefix(){
        return getPexPrefix() + " " + ChatColor.RESET + getName() + ChatColor.DARK_GRAY + " » " + ChatColor.RESET;
    }

    public Player getPlayer() {
        return player;
    }

    public UUID getUUID() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPlayer(Player player) {
        this.bossBar.addPlayer(player);
        this.player = player;
        this.permissionUser = PermissionsEx.getPermissionManager().getUser(player);
        this.name = player.getName();
    }

    public void cleanUp(){
        this.bossBar.removePlayer(player);
    }

    public PermissionUser getPermissionUser() {
        return permissionUser;
    }

    public void setPermissionUser(PermissionUser permissionUser) {
        this.permissionUser = permissionUser;
        this.player = permissionUser.getPlayer();
        this.name = player.getName();
    }

    public boolean isOnline(){
        return player != null && player.isOnline();
    }

    public void sendBossBar(BarColor barColor, String message){
        if(bossBarCallBack != null){
            bossBarCallBack.cancel();
        }
        bossBar.setTitle(message);
        bossBar.setProgress(0.0);
        bossBar.setVisible(true);
        bossBar.setColor(barColor);
        bossBarCallBack = new BukkitRunnable() {
            final double num = 0.025;
            double sofar = 0;
            public void run() {
                if((sofar += num) > 1){
                    cancel();
                    bossBar.setVisible(false);
                    bossBarCallBack = null;
                }
                else bossBar.setProgress(sofar);
            }
        };
        bossBarCallBack.runTaskTimerAsynchronously(MicroSMP.get(), 0, 1);
    }

    public void play(Sound sound){
        player.playSound(player.getLocation(), sound, 1f, 1f );
    }

    public boolean hasGroupPerm(String perm){
        for(PermissionGroup permissionGroup: permissionUser.getParents()){
            if(permissionGroup.has(perm)) return true;
        }
        return true;
    }

    public String getPexPrefix(){
        return permissionUser.getPrefix() == null ? "" : ChatColor.translateAlternateColorCodes('&', permissionUser.getPrefix());
    }

    public String getPexSuffix(){
        return permissionUser.getSuffix() == null ? "" : ChatColor.translateAlternateColorCodes('&', permissionUser.getSuffix());
    }

    public void setHeaderFooter(String headerstring, String footerstring){
        WrapperPlayServerPlayerListHeaderFooter playServerPlayerListHeaderFooter = new WrapperPlayServerPlayerListHeaderFooter();
        playServerPlayerListHeaderFooter.setHeader(WrappedChatComponent.fromText(headerstring));
        playServerPlayerListHeaderFooter.setFooter(WrappedChatComponent.fromText(footerstring));
        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(player, playServerPlayerListHeaderFooter.getHandle());
        } catch (Throwable e1) {
            MicroSMP.get().getLogger().log(Level.SEVERE,  "Could not send tab packet", e1);
        }
    }

    public FileConfiguration getConfig() {
        return config;
    }

    public void saveConfig() {
        try {
            config.save(userFile);
        } catch (Exception e) {
            MicroSMP.get().getLogger().severe(ChatColor.RED + "Could not save user data file: " + userFile.getName() + "!");
        }

        reloadConfig();
    }

    public void reloadConfig() {
        try {
            config = new YamlConfiguration();
            config.load(userFile);
        } catch (Exception ex) {
            MicroSMP.get().getLogger().severe("Could not load user data file: " + ex.getMessage());
        }
    }


}
