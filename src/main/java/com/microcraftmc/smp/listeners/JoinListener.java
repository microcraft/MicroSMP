package com.microcraftmc.smp.listeners;

import com.microcraftmc.smp.bossbar.BarManager;
import com.microcraftmc.smp.discord.DiscordBot;
import com.microcraftmc.smp.discord.utils.Bot;
import com.microcraftmc.smp.discord.utils.ChannelLogger;
import com.microcraftmc.smp.discord.utils.Chat;
import com.microcraftmc.smp.user.UserManager;
import com.microcraftmc.smp.MicroSMP;
import net.dv8tion.jda.core.MessageBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;


public class JoinListener implements Listener {


    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        event.setJoinMessage(null);

        UserManager.getInstance().procesJoin(player);

        BarManager.broadcast(BarColor.GREEN, ChatColor.DARK_GREEN + ChatColor.BOLD.toString() + "[+] " + ChatColor.RESET + player.getName());
        BarManager.broadcastSound(Sound.BLOCK_NOTE_BASS);

        if (DiscordBot.getInstance().isEnabled()) {
            ChannelLogger.logGuildMessage(new MessageBuilder().setEmbed(Chat.getEmbed().setTitle("Player Join", null)
                    .setDescription(player.getName() + " has joined the server.")
                    .setFooter("System time | " + Bot.getBotTime(), null)
                    .setColor(Chat.CUSTOM_GREEN).build()));
        }

    }

    @EventHandler
    public void onDisconnect(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        event.setQuitMessage(null);

        UserManager.getInstance().processLeave(player);

        BarManager.broadcast(BarColor.RED, ChatColor.DARK_RED + ChatColor.BOLD.toString() + "[-] " + ChatColor.RESET + player.getName());
        BarManager.broadcastSound(Sound.BLOCK_NOTE_BASS);

        if (DiscordBot.getInstance().isEnabled()) {
            ChannelLogger.logGuildMessage(new MessageBuilder().setEmbed(Chat.getEmbed().setTitle("Player Quit", null)
                    .setDescription(player.getName() + " has left the server.")
                    .setFooter("System time | " + Bot.getBotTime(), null)
                    .setColor(Chat.CUSTOM_RED).build()));
        }

    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {
        UserManager.getInstance().processLeave(event.getPlayer());
    }

}
