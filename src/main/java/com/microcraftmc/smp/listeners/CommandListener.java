package com.microcraftmc.smp.listeners;

import com.microcraftmc.smp.discord.DiscordBot;
import com.microcraftmc.smp.discord.utils.Bot;
import com.microcraftmc.smp.discord.utils.ChannelLogger;
import com.microcraftmc.smp.discord.utils.Chat;
import net.dv8tion.jda.core.MessageBuilder;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class CommandListener implements Listener {

    @EventHandler
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();

        if (DiscordBot.getInstance().isEnabled()) {
            ChannelLogger.logGuildMessage(new MessageBuilder().setEmbed(Chat.getEmbed().setTitle("Command Logger", null)
                    .setDescription(event.getMessage() + "\n" + "player: " + player.getName())
                    .setFooter("System time | " + Bot.getBotTime(), null)
                    .setColor(Chat.CUSTOM_ORANGE).build()));
        }

        if (event.getMessage().startsWith("/me") && !player.hasPermission("microcraft.admin")) {
            event.setCancelled(true);
            player.sendMessage(ChatColor.RED + "You do not have access to that command.");
        }

        if (event.getMessage().startsWith("/bukkit:") && !player.hasPermission("uhc.admin")) {
            event.setCancelled(true);
            player.sendMessage(ChatColor.RED + "You do not have access to that command.");
        }

        if (event.getMessage().startsWith("/pl") && !event.getMessage().startsWith("/playsound") && !player.hasPermission("microcraft.admin")) {
            event.setCancelled(true);
            player.sendMessage(ChatColor.RED + "You do not have access to that command.");
        }

        if (event.getMessage().startsWith("/plugins") && !player.hasPermission("microcraft.admin")) {
            event.setCancelled(true);
            player.sendMessage(ChatColor.RED + "You do not have access to that command.");
        }

        if (event.getMessage().startsWith("/minecraft:") && !player.hasPermission("microcraft.admin")) {
            event.setCancelled(true);
            player.sendMessage(ChatColor.RED + "You do not have access to that command.");
        }

    }

}
