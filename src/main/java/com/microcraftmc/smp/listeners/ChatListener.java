package com.microcraftmc.smp.listeners;


import com.microcraftmc.smp.bossbar.BarManager;
import com.microcraftmc.smp.config.ConfigManager;
import com.microcraftmc.smp.message.PrefixType;
import com.microcraftmc.smp.user.User;
import com.microcraftmc.smp.user.UserManager;
import com.microcraftmc.smp.MicroSMP;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;


public class ChatListener implements Listener {

    private MicroSMP plugin;
    private boolean globalmute;

    public ChatListener(MicroSMP plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        User user = UserManager.getInstance().getUserMap().get(player.getUniqueId());

        if (isGlobalmuted()) {
            if (!user.getPlayer().hasPermission("microcraft.chat.bypass")) {
                event.setCancelled(true);
                player.sendMessage("Global chat is currently disabled.");
            }
        }

        if (user.getPlayer().hasPermission("microcraft.chat.color")) {
            event.setFormat(user.getMessagePrefix() + ChatColor.translateAlternateColorCodes('&', event.getMessage()));
        } else {
            event.setFormat(user.getMessagePrefix() + event.getMessage());
        }
    }

    public void setGlobalmute(boolean enabled) {
        globalmute = enabled;

        //update config
        ConfigManager.getConfig().set("globalmute", enabled);
        ConfigManager.getInstance().saveConfig();
    }

    public boolean isGlobalmuted() {
        return globalmute;
    }

    public void toggleChat(){
        if (isGlobalmuted()) {
            unmuteChat();
        } else {
            muteChat();
        }
    }

    public void muteChat() {
        setGlobalmute(true);

        BarManager.broadcast(PrefixType.MAIN, BarColor.BLUE, ChatColor.BLUE + "Global mute enabled.");
        BarManager.broadcastSound(Sound.BLOCK_NOTE_BASS);
    }

    public void unmuteChat() {
        setGlobalmute(false);

        BarManager.broadcast(PrefixType.MAIN, BarColor.BLUE, ChatColor.BLUE + "Global mute disabled.");
        BarManager.broadcastSound(Sound.BLOCK_NOTE_BASS);
    }

}
