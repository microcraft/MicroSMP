package com.microcraftmc.smp;

import com.microcraftmc.smp.bossbar.BarManager;
import com.microcraftmc.smp.commands.CommandHandler;
import com.microcraftmc.smp.config.ConfigManager;
import com.microcraftmc.smp.discord.DiscordBot;
import com.microcraftmc.smp.listeners.ChatListener;
import com.microcraftmc.smp.listeners.CommandListener;
import com.microcraftmc.smp.listeners.JoinListener;
import com.microcraftmc.smp.message.MessageManager;
import com.microcraftmc.smp.user.UserManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class MicroSMP extends JavaPlugin {

    private static MicroSMP instance;

    //users data folder
    private File userFolder;

    //listeners
    private ChatListener chatListener;

    //commands
    private CommandHandler commandHandler;

    public void onEnable() {
        instance = this;

        //setup config manager and message manager
        ConfigManager.getInstance().setup();
        MessageManager.getInstance().setup();

        //check for existence of user folder
        userFolder = new File(getDataFolder() + File.separator + "users" + File.separator);
        if (!userFolder.exists()) {
            userFolder.mkdir();
        }

        //setup bossbar
        BarManager.getInstance().setup();

        //register listeners
        chatListener = new ChatListener(this);
        registerListeners(Bukkit.getPluginManager());

        //register commands
        commandHandler = new CommandHandler(this);
        commandHandler.registerCommands();

        //add all users currently online to the user map
        for (Player online : Bukkit.getOnlinePlayers()) {
            UserManager.getInstance().procesJoin(online);
        }

        //attempt to setup discord
        DiscordBot.getInstance().setup();
    }

    public void onDisable() {
        //disable needed stuff
        BarManager.getAnnounceBar().setVisible(false);
        BarManager.getAnnounceBar().removeAll();
        UserManager.getInstance().getUserMap().clear();
        DiscordBot.getInstance().shutdown();
    }

    public static MicroSMP get() {
        return instance;
    }

    public File getUserFolder() {
        return userFolder;
    }

    private void registerListeners(PluginManager pm) {
        pm.registerEvents(chatListener, this);
        pm.registerEvents(new JoinListener(), this);
        pm.registerEvents(new CommandListener(), this);
    }

    public ChatListener getChatListener() {
        return chatListener;
    }

}
