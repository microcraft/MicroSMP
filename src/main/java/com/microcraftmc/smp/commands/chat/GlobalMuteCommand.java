package com.microcraftmc.smp.commands.chat;

import com.microcraftmc.smp.commands.AbstractCommand;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;


public class GlobalMuteCommand extends AbstractCommand {

	public GlobalMuteCommand() {
		super("globalmute", "");
	}

	@Override
	public boolean execute(final CommandSender sender, final String[] args) throws CommandException {

		if (!(sender instanceof Player)) {
			throw new CommandException("Only players can global mute.");
		}

		plugin.getChatListener().toggleChat();

		return true;
	}

	@Override
	public List<String> tabComplete(CommandSender sender, String[] args) {
		return new ArrayList<>();
	}
}