package com.microcraftmc.smp.commands.admin;

import com.microcraftmc.smp.MicroSMP;
import com.microcraftmc.smp.commands.AbstractCommand;
import com.microcraftmc.smp.tasks.UpdateCountdownTask;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public class UpdateCommand extends AbstractCommand {

    public UpdateCommand() {
        super("update", "");
    }

    @Override
    public boolean execute(final CommandSender sender, final String[] args) {

        UpdateCountdownTask.getInstance().runTaskTimer(MicroSMP.get(), 0, 20);

        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String[] args) {
        return new ArrayList<>();
    }

}
