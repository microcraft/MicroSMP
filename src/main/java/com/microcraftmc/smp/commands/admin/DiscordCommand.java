package com.microcraftmc.smp.commands.admin;

import com.microcraftmc.smp.commands.AbstractCommand;
import com.microcraftmc.smp.discord.DiscordBot;
import com.microcraftmc.smp.message.MessageManager;
import com.microcraftmc.smp.message.PrefixType;
import net.dv8tion.jda.core.entities.Channel;
import net.dv8tion.jda.core.entities.User;
import com.microcraftmc.smp.exceptions.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;


public class DiscordCommand extends AbstractCommand {

	public DiscordCommand() {
		super("discord", "<token|serverLogId|masterId|statusId|prefix|blacklisted|trusted|status|connect> [add|remove] <value>");
	}

	@Override
	public boolean execute(final CommandSender sender, final String[] args) throws CommandException {

		if (args.length == 0) {
			return false;
		}

		if (args[0].equalsIgnoreCase("token")) {
			String token = args[1];
			DiscordBot.getInstance().setToken(token);
			sender.sendMessage(MessageManager.getPrefix(PrefixType.MAIN) + "the bot token has been set to " + token);
			return true;
		}

		if (args[0].equalsIgnoreCase("serverLogId")) {
			String serverLogId = args[1];

			if (DiscordBot.getInstance().isEnabled()) {
				Channel channel = DiscordBot.getClient().getTextChannelById(serverLogId);
				if (channel == null) {
					throw new CommandException("Invalid channel id provided");
				}

				DiscordBot.getInstance().setServerLogId(serverLogId);
				sender.sendMessage(MessageManager.getPrefix(PrefixType.MAIN) +
						"The server log channel has been set to " + channel.getName());
				sender.sendMessage(MessageManager.getPrefix(PrefixType.ARROW) + "id: " + serverLogId);
				return true;

			} else {
				DiscordBot.getInstance().setServerLogId(serverLogId);
				sender.sendMessage(MessageManager.getPrefix(PrefixType.MAIN) + "The server log channel has been set to " + serverLogId);
				return true;
			}

		}

		if (args[0].equalsIgnoreCase("masterId")) {
			String masterId = args[1];

			if (DiscordBot.getInstance().isEnabled()) {

				User discordUser = DiscordBot.getClient().getUserById(masterId);
				if (discordUser == null) {
					throw new CommandException("Invalid discord user id provided");
				}

				DiscordBot.getInstance().setMasterId(masterId);
				sender.sendMessage(MessageManager.getPrefix(PrefixType.MAIN) + "The bots master has been set to " + discordUser.getName());
				sender.sendMessage(MessageManager.getPrefix(PrefixType.ARROW) + "id: " + masterId);
				return true;

			} else {

				DiscordBot.getInstance().setMasterId(masterId);
				sender.sendMessage(MessageManager.getPrefix(PrefixType.MAIN) + "The bots master has been set to " + masterId);
				return true;
			}

		}

		if (args[0].equalsIgnoreCase("statusId")) {
			String statusId = args[1];

			if (DiscordBot.getInstance().isEnabled()) {
				Channel channel = DiscordBot.getClient().getTextChannelById(statusId);
				if (channel == null) {
					throw new CommandException("Invalid channel id provided");
				}

				DiscordBot.getInstance().setStatusId(statusId);
				sender.sendMessage(MessageManager.getPrefix(PrefixType.MAIN) +
						"The server status channel has been set to " + channel.getName());
				sender.sendMessage(MessageManager.getPrefix(PrefixType.ARROW) + "id: " + statusId);
				return true;

			} else {
				DiscordBot.getInstance().setStatusId(statusId);
				sender.sendMessage(MessageManager.getPrefix(PrefixType.MAIN) + "The server log channel has been set to " + statusId);
				return true;
			}

		}

		if (args[0].equalsIgnoreCase("prefix")) {
			String prefix = args[1];
			DiscordBot.getInstance().setPrefix(prefix);
			sender.sendMessage(MessageManager.getPrefix(PrefixType.MAIN) + "The prefix has been set to " + prefix);
			return true;
		}

		if (args[0].equalsIgnoreCase("blacklisted")) {

			return true;
		}

		if (args[0].equalsIgnoreCase("trusted")) {

			return true;
		}

		if (args[0].equalsIgnoreCase("status")) {

			if (DiscordBot.getInstance().isEnabled()) {
				sender.sendMessage(MessageManager.getPrefix(PrefixType.MAIN) + "Currently connected to discord");
			} else {
				sender.sendMessage(MessageManager.getPrefix(PrefixType.MAIN) + "Currently not connected to discord");
			}

			return true;
		}

		if (args[0].equalsIgnoreCase("connect")) {

			//verify we are not connected to discord
			if (DiscordBot.getInstance().isEnabled()) {
				return true;
			}

			sender.sendMessage(MessageManager.getPrefix(PrefixType.MAIN) + "Connecting to discord...");
			DiscordBot.getInstance().setup();

			return true;
		}

		return false;
	}

	@Override
	public List<String> tabComplete(CommandSender sender, String[] args) {
		List<String> toReturn = new ArrayList<>();

		if (args.length == 1) {
			toReturn.add("token");
			toReturn.add("serverLogId");
			toReturn.add("masterId");
			toReturn.add("statusId");
			toReturn.add("prefix");
			toReturn.add("blacklisted");
			toReturn.add("trusted");
			toReturn.add("status");
			toReturn.add("connect");
		}

		return  toReturn;
	}
}