package com.microcraftmc.smp.commands.basic;

import com.google.common.base.Joiner;
import com.microcraftmc.smp.bossbar.BarManager;
import com.microcraftmc.smp.commands.AbstractCommand;
import com.microcraftmc.smp.message.PrefixType;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.List;

public class BroadcastCommand extends AbstractCommand {

    public BroadcastCommand() {
        super("broadcast", "<message>");
    }

    @Override
    public boolean execute(final CommandSender sender, final String[] args) {
        if (args.length == 0) {
            return false;
        }

        String message = Joiner.on(' ').join(Arrays.copyOfRange(args, 0, args.length));

        //broadcast the message
        BarManager.broadcast(PrefixType.MAIN, BarColor.BLUE, ChatColor.translateAlternateColorCodes('&', message));
        BarManager.broadcastSound(Sound.BLOCK_NOTE_BASS);

        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String[] args) {
        return allPlayers();
    }

}
