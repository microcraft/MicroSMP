package com.microcraftmc.smp.message;

public enum PrefixType {

    MAIN,
    ARROW,
    ALERT,
    PERMS,
    JOIN,
    QUIT

}
