package com.microcraftmc.smp.discord;


import com.microcraftmc.smp.MicroSMP;
import com.microcraftmc.smp.config.ConfigManager;
import com.microcraftmc.smp.discord.commands.BotCmdHandler;
import com.microcraftmc.smp.discord.events.ServerEvents;
import com.microcraftmc.smp.discord.utils.Bot;
import com.microcraftmc.smp.discord.utils.ChannelLogger;
import com.microcraftmc.smp.discord.utils.Chat;
import com.microcraftmc.smp.discord.utils.IDiscordClient;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.Channel;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.dv8tion.jda.core.requests.RestAction;

import javax.security.auth.login.LoginException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class DiscordBot {

    private static DiscordBot instance;
    private static boolean enabled = false;
    private String token;
    private String serverLogId;
    private String statusId;
    private String masterId;
    private String prefix;
    private List<String> blacklisted = new ArrayList<>();
    private List<String> trusted = new ArrayList<>();

    //discord
    private static IDiscordClient discord;
    private static JDA client;
    private BotCmdHandler cmdHandler;
    private final static List<ListenerAdapter> listeners = new ArrayList<>();

    public static DiscordBot getInstance() {
        if (instance == null) {
            instance = new DiscordBot();
        }
        return instance;
    }

    public void setup() {
        if (!ConfigManager.getDiscord().isString("token")) {
            return;
        }

        token = ConfigManager.getDiscord().getString("token", "");
        prefix = ConfigManager.getDiscord().getString("prefix", "!");
        masterId = ConfigManager.getDiscord().getString("masterId", "");
        statusId = ConfigManager.getDiscord().getString("statusId", "");
        serverLogId = ConfigManager.getDiscord().getString("serverLogId", "");

        if (ConfigManager.getDiscord().isList( "blacklisted")) {
            List<String> blacklistedConfig = ConfigManager.getConfig().getStringList("blacklisted");
            blacklisted.addAll(blacklistedConfig);
        }

        if (ConfigManager.getDiscord().isList( "trusted")) {
            List<String> trustedConfig = ConfigManager.getConfig().getStringList("trusted");
            trusted.addAll(trustedConfig);
        }

        connect();
    }

    public void connect() {

        RestAction.DEFAULT_FAILURE = t -> {};
        discord = new IDiscordClient();
        cmdHandler = new BotCmdHandler();

        try {

            try {
                //add .addEventListener(new ChatEvents(), new ServerEvents(), commandHandler)
                client = new JDABuilder(AccountType.BOT)
                        .setToken(getToken())
                        .setGame(Game.of("loading..."))
                        .setStatus(OnlineStatus.IDLE)
                        .buildAsync();

                //register listeners
                addListener(cmdHandler);
                addListener(new ServerEvents());

            } catch (RateLimitedException e) {
                try {
                    Thread.sleep(e.getRetryAfter());
                } catch (Exception ex) {
                    //do nothing
                }
            }

            cmdHandler.registerCommands();

        } catch (LoginException e) {
            MicroSMP.get().getLogger().log(Level.SEVERE, "Could not login to Discord!", e);
            try {
                Thread.sleep(500);
            } catch (Exception ex) {
                //do nothing
            }
            shutdown();
        }


    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;

        ConfigManager.getDiscord().set("token", token);
        ConfigManager.getInstance().saveDiscord();
    }

    public String getServerLogId() {
        return serverLogId;
    }

    public void setServerLogId(String serverLogId) {
        this.serverLogId = serverLogId;

        ConfigManager.getDiscord().set("serverLogId", serverLogId);
        ConfigManager.getInstance().saveDiscord();
    }

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;

        ConfigManager.getDiscord().set("masterId", masterId);
        ConfigManager.getInstance().saveDiscord();
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;

        ConfigManager.getDiscord().set("statusId", statusId);
        ConfigManager.getInstance().saveDiscord();
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;

        ConfigManager.getDiscord().set("prefix", prefix);
        ConfigManager.getInstance().saveDiscord();
    }

    public List<String> getBlacklisted() {
        return blacklisted;
    }

    public void saveBlacklisted() {
        ConfigManager.getDiscord().set("blacklisted", getBlacklisted());
        ConfigManager.getInstance().saveDiscord();
    }

    public List<String> getTrusted() {
        return trusted;
    }

    public void saveTrusted() {
        ConfigManager.getDiscord().set("trusted", getTrusted());
        ConfigManager.getInstance().saveDiscord();
    }

    public static JDA getClient() {
        return client;
    }

    public static IDiscordClient getDiscord() {
        return discord;
    }

    public void shutdown() {
        if (enabled) {
            enabled = false;

            getClient().getPresence().setGame(Game.of("shutting down..."));
            getClient().getPresence().setStatus(OnlineStatus.DO_NOT_DISTURB);

            try {

                //TODO Remove all messages that are waiting on task timers
                logShutdown();

                removeListeners();

                client.shutdown();
                MicroSMP.get().getLogger().info("Client shutdown");

            } catch (Exception e) {
                MicroSMP.get().getLogger().log(Level.SEVERE, "Could not shutdown bot!", e);
                e.printStackTrace();
            }

        }
    }

    /**
     * Registers the listener
     *
     * @param listener the listener to register
     */
    public void addListener(ListenerAdapter listener) {
        listeners.add(listener);
        client.addEventListener(listener);
    }

    /**
     * Removes all listeners currently registered.
     */
    private void removeListeners() {
        for (ListenerAdapter listener : listeners) {
            client.removeEventListener(listener);
        }
    }

    public boolean isTrusted(Member member) {
        for (String id : getTrusted()) {
            if (getDiscord().userHasRoleId(member, id)) {
                return true;
            }
        }
        return false;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public static void setEnabled(boolean enable) {
        enabled = enable;
    }

    public static void logShutdown() {
        ChannelLogger.logStatusMessage(new MessageBuilder().setEmbed(Chat.getEmbed().setTitle("Server Status", null)
                .setDescription("The server is now offline")
                .setFooter("System time | " + Bot.getBotTime(), null)
                .setColor(Chat.CUSTOM_RED).build()));
    }

}
