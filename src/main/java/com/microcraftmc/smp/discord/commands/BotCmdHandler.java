package com.microcraftmc.smp.discord.commands;

import com.microcraftmc.smp.MicroSMP;
import com.microcraftmc.smp.discord.DiscordBot;
import com.microcraftmc.smp.discord.commands.general.WhitelistCommand;
import com.microcraftmc.smp.discord.utils.Chat;
import com.microcraftmc.smp.discord.utils.PermissionsUtil;
import com.microcraftmc.smp.exceptions.CommandException;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class BotCmdHandler extends ListenerAdapter {

    private static List<DiscordCmd> cmds = new ArrayList<>();

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {

        Message message = event.getMessage();
        Guild guild = event.getGuild();
        Member sender = event.getMember();
        User user = message.getAuthor();
        TextChannel channel = event.getChannel();


        if (message.getRawContent() != null && message.getContent().startsWith(DiscordBot.getInstance().getPrefix())) {
            if (DiscordBot.getInstance().getBlacklisted().contains(sender.getUser().getId())) {
                Chat.removeMessage(message);
                Chat.sendPM("You are blacklisted from using bot commands. " +
                        "If you believe this is an error, please contact spacetrain31.", sender.getUser());
                return;
            }

            String msg = event.getMessage().getRawContent();
            String command = msg.substring(1);
            String[] args = new String[0];
            if (msg.contains(" ")) {
                command = command.substring(0, msg.indexOf(" ") - 1);
                args = msg.substring(msg.indexOf(" ") + 1).split(" ");
            }

            DiscordCmd cmd = getCommand(command);

            if (cmd == null) return; //invalid command

            if (cmd.getType() == DiscordCmd.CommandType.MASTER && !PermissionsUtil.isBotMaster(sender.getUser())) {
                return;
            }

            if (cmd.getType() == DiscordCmd.CommandType.TRUSTED && !PermissionsUtil.isTrusted(guild, sender)) {
                return;
            }


            if (!cmd.isEnabled()) {
                //Command is not enabled, send user a message informing them
                sender.getUser().openPrivateChannel().queue(c -> c.sendMessage("The command " + cmd.getName() + " is currently disabled.").queue(m -> m.getPrivateChannel().close()));
                return;
            }

            try {
                if (!cmd.onCommand(guild, channel, sender, message, args)) {
                    Chat.sendMessage(sender.getAsMention() + " Usage: ```" + DiscordBot.getInstance().getPrefix() + cmd.getUsage() + "```", channel, 20);
                }
            } catch (CommandException e) {
                MicroSMP.get().getLogger().log(Level.SEVERE, e.getMessage(), e);
            }

        }

    }

    /**
     * Get a command.
     *
     * @param name The name of the command
     * @return The Command if found, null otherwise.
     */
    public DiscordCmd getCommand(String name) {
        for (DiscordCmd cmd : cmds) {
            if (cmd.getAliases().length > 0) {
                for (String alias : cmd.getAliases()) {
                    if (cmd.getName().equalsIgnoreCase(name) || alias.equalsIgnoreCase(name)) {
                        return cmd;
                    }
                }
            } else {
                if (cmd.getName().equalsIgnoreCase(name)) {
                    return cmd;
                }
            }
        }
        return null;
    }

    public void registerCommands() {

        //general
        cmds.add(new WhitelistCommand());

        //management

        //master

    }


}
