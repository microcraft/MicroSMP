package com.microcraftmc.smp.discord.commands.general;

import com.microcraftmc.smp.MicroSMP;
import com.microcraftmc.smp.discord.commands.DiscordCmd;
import com.microcraftmc.smp.discord.utils.Chat;
import com.microcraftmc.smp.exceptions.CommandException;
import com.microcraftmc.smp.utils.uuid.UUIDFetcher;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.UUID;
import java.util.logging.Level;

public class WhitelistCommand extends DiscordCmd
{

    public WhitelistCommand() {
        super("whitelist", CommandType.GENERAL, "username");
    }

    @Override
    public boolean onCommand(Guild guild, TextChannel channel, Member sender, Message message, String[] args) throws CommandException {

        if (args.length == 0) {
            return false;
        }

        UUID uuid;

        try {
            uuid = UUIDFetcher.getUUIDOf(args[0]);
        } catch (Exception ex) {
            Chat.sendMessage(sender.getAsMention() + " A minecraft account with that username could not be found.", channel, 20);
            return true;
        }

        //whitelist the player
        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "whitelist add " + args[0]);
        try {
            OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
            MicroSMP.get().getLogger().log(Level.INFO, "Whitelisted " + player.getName());
        } catch (Exception ex) {
            MicroSMP.get().getLogger().log(Level.WARNING, "Could not whitelist user", ex);
        }

        Chat.sendMessage(sender.getAsMention() + " You have been whitelisted and may now join the server.", channel, 20);

        return true;
    }

}