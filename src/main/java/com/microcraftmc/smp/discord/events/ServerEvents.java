package com.microcraftmc.smp.discord.events;

import com.microcraftmc.smp.MicroSMP;
import com.microcraftmc.smp.discord.DiscordBot;
import com.microcraftmc.smp.discord.utils.Bot;
import com.microcraftmc.smp.discord.utils.ChannelLogger;
import com.microcraftmc.smp.discord.utils.Chat;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.events.DisconnectEvent;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.events.ReconnectedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class ServerEvents extends ListenerAdapter {

    @Override
    public void onReady(ReadyEvent event) {

        Bot.updateUsers();
        DiscordBot.getClient().getPresence().setStatus(OnlineStatus.ONLINE);

        DiscordBot.setEnabled(true);

        MicroSMP.get().getLogger().info("Bot ready.");
        ChannelLogger.logGuildMessage("Bot ready!");

        ChannelLogger.logStatusMessage(new MessageBuilder().setEmbed(Chat.getEmbed().setTitle("Server Status", null)
                .setDescription("The server is now online, you may now connect")
                .setFooter("System time | " + Bot.getBotTime(), null)
                .setColor(Chat.CUSTOM_GREEN).build()));

    }

    @Override
    public void onReconnect(ReconnectedEvent event) {
        MicroSMP.get().getLogger().info("Connection to Discord has been reestablished!");
    }

    @Override
    public void onDisconnect(DisconnectEvent event) {
        MicroSMP.get().getLogger().warning("Disconnected from Discord.");
    }

}
