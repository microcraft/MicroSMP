package com.microcraftmc.smp.discord.utils;

import com.microcraftmc.smp.discord.DiscordBot;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.User;

public class PermissionsUtil {

    public static boolean isBotMaster(User user) {
        return user.equals(Bot.getMster());
    }

    public static boolean hasAdministrator(Member member) {
        return member.hasPermission(Permission.ADMINISTRATOR);
    }

    public static boolean isTrusted(Guild guild, Member member) {
        return DiscordBot.getInstance().isTrusted(member) || isBotMaster(member.getUser());
    }

}
