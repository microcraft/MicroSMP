package com.microcraftmc.smp.discord.utils;

import com.microcraftmc.smp.discord.DiscordBot;
import net.dv8tion.jda.core.MessageBuilder;

public class ChannelLogger {

    public static void logGuildMessage(String message) {
        if (DiscordBot.getInstance().getServerLogId() != null) {
            Chat.sendMessage(message, DiscordBot.getClient().getTextChannelById(DiscordBot.getInstance().getServerLogId()));
        }
    }

    public static void logGuildMessage(MessageBuilder message) {
        if (DiscordBot.getInstance().getServerLogId() != null) {
            DiscordBot.getClient().getTextChannelById(DiscordBot.getInstance().getServerLogId()).sendMessage(message.build()).queue();
        }
    }

    public static void logStatusMessage(String message) {
        if (DiscordBot.getInstance().getStatusId() != null) {
            Chat.sendMessage(message, DiscordBot.getClient().getTextChannelById(DiscordBot.getInstance().getStatusId()));
        }
    }

    public static void logStatusMessage(MessageBuilder message) {
        if (DiscordBot.getInstance().getStatusId() != null) {
            DiscordBot.getClient().getTextChannelById(DiscordBot.getInstance().getStatusId()).sendMessage(message.build()).queue();
        }
    }

}
