package com.microcraftmc.smp.discord.utils;

import com.microcraftmc.smp.discord.DiscordBot;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class IDiscordClient {

    public boolean isReady() {
        return DiscordBot.getClient().getStatus().equals(JDA.Status.CONNECTED);
    }

    public List<Guild> getRolesForGuild(Guild guild) {
        return DiscordBot.getClient().getGuilds().stream().filter(g -> g.getRoles().equals(guild)).collect(Collectors.toList());
    }

    public boolean userHasRoleId(Member member, String id) {
        return member.getRoles().contains(DiscordBot.getClient().getRoleById(id));
    }

    public void streaming(String status, String url) {
        DiscordBot.getClient().getPresence().setGame(Game.of(status, url));
    }

}
